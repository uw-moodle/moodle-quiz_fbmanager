<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Feedback manager quiz report.
 *
 * @package     quiz_fbmanager
 * @copyright   2014 University of Wisconsin
 * @author      Nick Koeppen, Matt Petro
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot.'/local/fm/renderer.php');

class quiz_fbmanager_renderer extends local_fm_renderer {

    protected function render_quiz_fbmanager_assign_report(quiz_fbmanager_assign_report $report){
        $assigntable = $report->get_table();

        echo $this->output->container_start('', 'quiz_fbmanager_assign_report');
        $helpicon = $this->output->help_icon('feedbackassign', 'quiz_fbmanager');
        echo $this->output->container_start('centerpara');
        echo $this->output->heading(get_string('feedbackassign', 'quiz_fbmanager').$helpicon);
        echo $this->output->container_end();

        echo $this->output->container('', '', 'quiz_fbmanager_assign_sidebar_marker');
        echo $this->output->container_start('', 'quiz_fbmanager_assign_sidebar');
        echo $this->output->container_start('contents');
        $this->render($assigntable->get_component('filters'));

        $this->render($assigntable->get_component('feedback'));

        echo $this->output->container_end();
        echo $this->output->container_end();

        $this->render($assigntable->get_component('questions'));

        $this->render($assigntable);

        echo $this->output->container_end();
    }

    protected function render_quiz_fm_assign_table(quiz_fm_assign_table $table){
        echo $this->render_local_fm_assign_table($table);
    }

    protected function render_quiz_fbmanager_feedback_report(quiz_fbmanager_feedback_report $report){
        $reportdefinition = $report->get_definition();
        $settingform = $report->get_settings_form();

        $data = $report->get_data_for_display();

        // See if we have anything to show
        $hasfeedback = false;
        foreach ($data as $qdata) {
            if (!empty($qdata['feedbackbyresponse'])) {
                $hasfeedback = true;
                break;
            }
        }

        echo $this->output->container_start('', 'quiz_fbmanager_report');
        echo $this->output->heading(get_string('feedbackreport', 'quiz_fbmanager'));
        echo $this->output->box(get_string('feedbackreportintro', 'quiz_fbmanager'));

        if ($settingform->edit_report()) {
            $editform = $report->get_definition_edit_form();
        } else if ($settingform->save_report() || !$reportdefinition->exists()) {
            $html = html_writer::start_tag('ol');
            foreach($data as $qid => $qdata){

                // We use inline styles here to make the report work better with the WYSIWYG editor.

                if (!empty($qdata['feedbackbyresponse'])) {
                    // Question summary
                    $html .= html_writer::start_tag('li');
                    $html .= html_writer::start_tag('div', array('style'=>'margin-left: 30px;'));
                    // As in question_definition::get_question_summary()
                    $question_summary = question_utils::to_plain_text($qdata['question']->questiontext, $qdata['question']->questiontextformat);
                    $summary = html_writer::tag('p', $question_summary);
                    $html .= $summary;
                    $html .= '<br />';

                    if ($report->get_report_type() == local_fm_report::TYPE_FEEDBACK) {
                        $html .= $this->render_question_data_by_feedback($report, $qdata);
                    } else {
                        $html .= $this->render_question_data_by_response($report, $qdata);
                    }
                }
                $html .= html_writer::end_tag('li');
            }
            $html .= html_writer::end_tag('ol');
        } else {
            $html = $reportdefinition->format_text();
        }

        // Display report
        if ($settingform->edit_report()) {
            $editform->display();
        } else {
            $settingform->display();
        }

        if ($settingform->save_report()) {
            $isnew = !$reportdefinition->exists();
            $newdef       = new stdClass();
            $newdef->text = array(
                'text'    => $html,
                'format'  => FORMAT_HTML,
            );
            $reportdefinition->form_load_data($newdef);
            $reportdefinition->save();

            $message = get_string('changessaved');
            if ($isnew) {
                global $PAGE;
                $link = html_writer::link($PAGE->url->out(false, array('edit' => true)), get_string('edit'));
                $message .= " ($link)";
            }
            echo $this->output->notification($message, 'notifysuccess');
        }

        if (!$settingform->edit_report()) {
            echo $this->output->container_start('', 'quiz_fbmanager_report_contents');
            if (!$hasfeedback) {
                echo $this->output->container_start('', 'quiz_fbmanager_report');
                echo $this->output->error_text(get_string('notypicalfeedback', 'quiz_fbmanager'));
                echo $this->output->container_end();
            }
            echo $html;
            echo $this->output->container_end();
        }


        echo $this->output->container_end();

    }

    protected function render_question_data_by_feedback(quiz_fbmanager_feedback_report $report, array $qdata) {
        $manager = $report->get_manager();
        $showfeedbackcount = $report->show_feedback_count();
        $showallresponses = $report->show_allresponses();
        $feedbackstr = get_string('feedback', 'quiz_fbmanager').": ";
        if ($showallresponses) {
            $responsestr = get_string('studentresponses', 'quiz_fbmanager').":";
        } else {
            $responsestr = get_string('typicalstudentresponses', 'quiz_fbmanager').":";
        }
        $responses = array();
        if (isset($qdata['responsebyfeedback'])) {
            $responses = $qdata['responsebyfeedback'];
        }
        if (isset($qdata['responsebycustom'])) {
            $responses = array_merge($responses, $qdata['responsebycustom']);
        }
        if (isset($qdata['responsenofeedback'])) {
            $responses[] = $qdata['responsenofeedback'];
        }
        foreach($responses as $rdata){
            if (!empty($rdata['attempts'])) {
                $attempts = $rdata['attempts'];
            } else {
                $attempts = array();
            }
            if (!empty($rdata['custom'])) {
                $custom = $rdata['custom'];
            } else {
                $custom = null;
            }
            if (!empty($rdata['assign'])) {
                $assign = $rdata['assign'];
                $instance = $assign->get_feedback_instance();
                $fbtext = $instance->get_text();
                if (trim($fbtext) === '') {
                    $fbtext = $instance->get_name();
                }
                $fbtext = html_writer::tag('strong',$feedbackstr).$fbtext;
                $srtext = html_writer::tag('div', $responsestr, array('style'=>'margin-left:2em;margin-top:1em;'));
                if ($showfeedbackcount) {
                    $count = local_fm_feedback_assign::count_for_feedback_instance($instance->id);
                }
            } else if (!empty($rdata['custom'])) {
                $fbtext = html_writer::tag('strong',$feedbackstr).$rdata['custom']->get_text();
                $srtext = html_writer::tag('div', $responsestr, array('style'=>'margin-left:2em;margin-top:1em;'));
                $count = 1;
            } else {
                $fbtext = html_writer::tag('strong',get_string('responseswithnofeedback', 'quiz_fbmanager').":");
                $srtext = '';
                $count = false;
            }
            $text = '';
            $text .= $fbtext;
            if ($showfeedbackcount && $count) {
                $text .= html_writer::tag('span', '&nbsp;'.get_string('feedbackassigncount', 'quiz_fbmanager', $count), array('style' => 'font-weight:normal;'));
            }
            $ftext = html_writer::tag('div', $text);
            $ftext .= $srtext;
            $responses = array();
            foreach($attempts as $qaid => $attempt){
                $response = html_writer::tag('span', '&ldquo;'.$attempt->get_response_summary().'&rdquo;', array('style'=>'font-family: Georgia, serif;'));
                if ($report->show_users()) {
                    $group = $manager->get_groupmanager()->get_attempt_group($attempt);
                    $groupname = $group->get_name(null);
                    $response .= html_writer::tag('em', " -- $groupname");
                }
                if ($report->show_grades()) {
                    $maxmark = $attempt->get_max_mark();
                    $mark = $attempt->get_fraction() * $maxmark;
                    $response .= html_writer::tag('em', " &nbsp;&nbsp;".get_string('grade').": $mark / $maxmark");
                }
                $responses[] = html_writer::tag('li', $response, array('style'=>'margin-bottom: 1em;background-color:#f1f1f1;'));
            }
            $rtext = html_writer::tag('ul', implode('', $responses), array('style' => 'list-style-type: none;'));

            $html[] = $ftext.$rtext;
        }
        return join('', $html);
    }

    protected function render_question_data_by_response(quiz_fbmanager_feedback_report $report, array $qdata) {
        $manager = $report->get_manager();
        foreach($qdata['feedbackbyresponse'] as $qaid => $adata){
            $attempt = $adata['attempt'];

            $response = '';
            $response .= html_writer::tag('p', "Student response:");
            $response .= html_writer::start_tag('div', array('style'=>'margin-left:30px;'));
            $response .= html_writer::start_tag('p', array('style'=>'background-color:#f1f1f1;'));
            $response .= html_writer::tag('span', '&ldquo;'.$attempt->get_response_summary().'&rdquo;', array('style'=>'font-family: Georgia, serif;'));
            if ($report->show_users()) {
                $group = $manager->get_groupmanager()->get_attempt_group($attempt);
                //$attemptuser = $manager->get_user(local_fm_question_attempt_feedback::get_question_attempt_userid($attempt));
                //$groupname = $group->get_name($attemptuser);
                $groupname = $group->get_name(null);
                $response .= html_writer::tag('em', " -- $groupname");
            }
            if ($report->show_grades()) {
                $maxmark = $attempt->get_max_mark();
                $mark = $attempt->get_fraction() * $maxmark;
                $response .= html_writer::tag('em', " &nbsp;&nbsp;".get_string('grade').": $mark / $maxmark");
            }
            $response .= html_writer::end_tag('p');

            $feedback = array();
            if (!empty($adata['assigns'])) {
                foreach($adata['assigns'] as $assign){
                    $instance = $assign->get_feedback_instance();
                    $text = '';
                    $text .= html_writer::tag('strong', get_string('feedback', 'quiz_fbmanager').': ');
                    $fbtext = $instance->get_text();
                    if (trim($fbtext) === '') {
                        $fbtext = $instance->get_name();
                    }
                    $text .= $fbtext;

                    if ($report->show_feedback_count()) {
                        $count = local_fm_feedback_assign::count_for_feedback_instance($instance->id);
                        $text .= html_writer::tag('span', '&nbsp;'.get_string('feedbackassigncount', 'quiz_fbmanager', $count), array('style' => 'font-weight:normal;'));
                    }
                    $feedback[] = html_writer::tag('p', $text);
                }
            }
            if (!empty($adata['customs'])) {
                foreach($adata['customs'] as $custom){
                    $text = '';
                    $text .= html_writer::tag('strong', "Feedback: ");
                    $fbtext = $custom->get_text();
                    $text .= $fbtext;

                    if ($report->show_feedback_count()) {
                        $count = 1;
                        $text .= html_writer::tag('span', '&nbsp;'.get_string('feedbackassigncount', 'quiz_fbmanager', $count), array('style' => 'font-weight:normal;'));
                    }
                    $feedback[] = html_writer::tag('p', $text);
                }
            }

            $response .= implode('', $feedback);
            $response .= html_writer::end_tag('div');
            $html[] =  $response;
        }
        return join('', $html);
    }

    protected function render_quiz_fbmanager_send_report(quiz_fbmanager_send_report $report){
        global $PAGE;

        echo $this->output->container_start('', 'quiz_fbmanager_send_form');
        echo $this->output->heading(get_string('sendfeedback', 'quiz_fbmanager'));
        echo $this->output->box(get_string('sendfeedbackintro', 'quiz_fbmanager'));
        $report->get_send_form()->display();
        echo $this->output->container_end();

        if ($report->has_overlapping_groups()) {
            echo $this->output->notification(get_string('groupsnotdisjoint', 'quiz_fbmanager'));
        }
        // Add popup event handler for preview button

        $cm = $report->get_cm();
        $previewurl = new moodle_url('/mod/quiz/report/fbmanager/preview.php', array('cmid'=>$cm->id));

        // We only use the popup_action to fetch default js_options for the popup function
        $popupaction = new popup_action('click', $previewurl, 'popup', array('height'=>800, 'width'=>600));
        $config['baseurl'] = $previewurl->out(false);
        $config['options'] = $popupaction->get_js_options();
        $config['name'] = 'fbpreview';

        $this->page->requires->yui_module('moodle-quiz_fbmanager-preview', 'M.quiz_fbmanager.preview_popup', array($config));
    }

    protected function render_quiz_fbmanager_grade_report(quiz_fbmanager_grade_report $report){
        global $PAGE;

        echo $this->output->container_start('', 'quiz_fbmanager_grade_form');
        if ($report->has_overlapping_groups()) {
            echo $this->output->notification(get_string('groupsnotdisjoint', 'quiz_fbmanager'));
        }
        echo $this->output->heading(get_string('updatequizgrades', 'quiz_fbmanager'));
        echo $this->output->box(get_string('sendgradesintro', 'quiz_fbmanager'));
        $report->get_grade_form()->display();

        echo $this->output->container_end();
    }

    protected function render_quiz_fbmanager_settings_report(quiz_fbmanager_settings_report $report){
        global $PAGE;

        echo $this->output->container_start('', 'quiz_fbmanager_settings_form');
        if ($report->has_overlapping_groups()) {
            echo $this->output->notification(get_string('groupsnotdisjoint', 'quiz_fbmanager'));
        }
        $report->get_settings_form()->display();

        echo $this->output->container_end();
    }
}
