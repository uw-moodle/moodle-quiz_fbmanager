<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Feedback manager quiz report.
 *
 * @package     quiz_fbmanager
 * @copyright   2014 University of Wisconsin
 * @author      Nick Koeppen, Matt Petro
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot.'/local/fm/message/forms.php');

class quiz_fbmanager_send_report extends quiz_fbmanager_report_base {
    protected $_form;
    protected $_preview;

    function extend_navigation(moodle_page $page){
        $url = $page->url;
        $url->params($this->get_page_params());
        $page->navbar->add(get_string('sendtabname','quiz_fbmanager'), $url);
    }

    /**
     *
     * @return local_fm_send_form
     */
    function get_send_form(){
        global $PAGE;

        if (!isset($this->_form)) {
            $customdata = array('manager' => $this->manager);
            $this->_form = new local_fm_send_form($PAGE->url, $customdata);
        }

        return $this->_form;
    }

    /**
     *
     * @return local_fm_send_preview
     */
    function get_send_preview(){
        if (!isset($this->_preview)) {
            $this->_preview = new local_fm_send_preview($this->get_send_form());
        }

        return $this->_preview;
    }

    function get_page_params(){
        $params = parent::get_page_params();

        $paramname = local_fm_send_preview::PARAM_GROUP;
        $userid = optional_param($paramname, null, PARAM_INT);
        if ($userid) {
            $params[$paramname] = $userid;
        }

        return $params;
    }

    function get_url_template(){
        global $PAGE;

        $params = array(
            'type'    => $this->manager->get_typename(),
            'context' => $this->manager->get_context()->id,
            'return'  => $PAGE->url,
        );
        return new moodle_url('/local/fm/message/template.php', $params);
    }

    function get_url_preview(){
        $sendform = $this->get_send_form();
        $params = array('cmid'=>$this->manager->get_cm()->id,
                        'assigners' => implode(',', $sendform->get_assigner_ids()),
                        'questions' => implode(',', $sendform->get_question_ids()));
        return new moodle_url('/mod/quiz/report/fbmanager/preview.php', $params);
    }

    function process_submission(){
        global $PAGE;

        $sendform = $this->get_send_form();
        $return   = $PAGE->url;

        if ($sendform->is_cancelled()) {
            redirect($return);
        } else if ($data = $sendform->get_data()) {
            if (!empty($data->preview)) {
                $sendform->update_configuration();
                $previewurl = $this->get_url_preview();
                redirect($previewurl);
            }
            if (!empty($data->edittemplate)) {
                $sendform->update_configuration();
                $url = $this->get_url_template();
                redirect($url);
            }
            if (!empty($data->send)) {
                $success = $sendform->send_feedback();
                $message = $success ? get_string('feedbacksendok', 'quiz_fbmanager') : get_string('feedbacksenderror', 'quiz_fbmanager');
                redirect($return, $message);
            }
        }
    }

    function has_overlapping_groups() {
        return $this->manager->get_groupmanager()->has_overlapping_groups();
    }

}