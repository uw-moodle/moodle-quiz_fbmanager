<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Feedback manager quiz report.
 *
 * @package     quiz_fbmanager
 * @copyright   2014 University of Wisconsin
 * @author      Nick Koeppen, Matt Petro
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot.'/mod/quiz/locallib.php');
require_once($CFG->dirroot.'/local/fm/locallib.php');

class quiz_fm_manager extends local_fm_manager {

    /**
     * @var quiz
     */
    protected $_quiz;
    protected $_questions;
    protected $_settings;

    const GRADEHIGHEST = 1;
    const GRADEAVERAGE = 2;
    const ATTEMPTFIRST = 3;
    const ATTEMPTLAST = 4;

    function __construct($contextorid, $questionid = null){
        parent::__construct($contextorid, $questionid);

        if (! ($this->context instanceof context_module)) {
            throw new Exception('invalidcontext');
        }
    }

    function verify_page(){
        parent::verify_page();

        // Verify question belongs to quiz.
        if ($this->questionid) {
            $questions = $this->get_questions();
            if (!isset($questions[$this->questionid])) {
                throw new moodle_exception('Invalid question id.');
            }
        }
    }

    function get_cm(){
        return $this->get_quiz()->get_cm();
    }

    function get_instance_settings() {
        global $DB;

        if ($this->_settings) {
            $settings = $this->_settings;
        } else {
            $quizid = $this->get_quiz()->get_quizid();
            $settings = $DB->get_record('quiz_fbmanager', array('quizid'=>$quizid));
            if (!$settings) {
                $settings = new stdClass();
                $settings->quizid = $quizid;
                $settings->id = $DB->insert_record('quiz_fbmanager', $settings);
                $settings = $DB->get_record('quiz_fbmanager', array('quizid'=>$quizid));

            }
            $this->_settings = $settings;
        }
        return $settings;
    }

    function save_instance_settings($settings) {
        global $DB;

        $DB->update_record('quiz_fbmanager', $settings);
    }

    function get_groupingid() {
        return $this->get_instance_settings()->groupingid;
    }

    function get_message_data($userto, $userfrom, $attempts, $allassigns, $format = null){
        $data = parent::get_message_data($userto, $userfrom, $attempts, $allassigns, $format);

        $quizdef = $this->get_quiz_definition();
        $data['$quizname'] = $quizdef->name;

        $url = new moodle_url('/mod/quiz/view.php', array('id' => $this->get_cm()->id));
        if ($format == FORMAT_HTML) {
            $data['$quizurl'] = html_writer::link($url, $url);
        } else {
            $data['$quizurl'] = $url->out(false);
        }

        $grades = quiz_get_user_grades($quizdef, $userto->id);
        $grade = reset($grades);
        if ($grade) {
            $data['$quizgrade'] = quiz_format_grade($quizdef, $grade->rawgrade);
        } else {
            $data['$quizgrade'] = '';
        }
        $data['$quizmaxgrade'] = $quizdef->grade;

        return $data;
    }

    function get_message_variables(){
        $vars = parent::get_message_variables();
        $vars['context'][] = '$quizname';
        $vars['context'][] = '$quizurl';
        $vars['context'][] = '$quizgrade';
        $vars['context'][] = '$quizmaxgrade';

        return $vars;
    }

    function get_navigation_baseurl(){
        $navparams = array('id' => $this->get_cm()->id, 'mode' => 'fbmanager');
        return new moodle_url('/mod/quiz/report.php', $navparams);
    }

    function get_page_renderer(moodle_page $page){
        global $CFG;

        require_once($CFG->dirroot.'/mod/quiz/report/fbmanager/renderer.php');

        return $page->get_renderer('quiz_fbmanager');
    }

    protected function get_qubaid_condition($conditions = null){
        global $DB;

        $from = '{quiz_attempts} quiza';
        $where = 'quiza.quiz = :quizaquiz AND quiza.preview = 0';
        $params = array('quizaquiz' => $this->get_quiz()->get_quizid());
        if (!empty($conditions['users'])) {
            list ($usersql, $userparams) = $DB->get_in_or_equal($conditions['users'], SQL_PARAMS_NAMED);
            $where .= " AND quiza.userid $usersql";
            $params = array_merge($params, $userparams);
        }
        if (!empty($conditions['attempts'])) {
            list ($attemptsql, $attemptparams) = $DB->get_in_or_equal($conditions['attempts'], SQL_PARAMS_NAMED);
            $where .= " AND quiza.id $attemptsql";
            $params = array_merge($params, $attemptparams);
        }
        return new qubaid_join($from, 'quiza.uniqueid', $where, $params);
    }

    function get_question_usage_component(){
        return 'mod_quiz';
    }

    function get_questions(){
        if (!isset($this->_questions)) {
            $quiz = $this->get_quiz();

            // First load quiz questions.
            if ($quiz->has_questions()) {
                $quiz->preload_questions();
                $quiz->load_questions();
            }
            $quizquestions = $quiz->get_questions();

            // Next load subquestions for random questions.
            $randomqids = array();
            foreach ($quizquestions as $questionid => $questiondata) {
                if ($questiondata->qtype === 'random') {
                    $newquestionids = question_bank::get_qtype('random', true)->get_available_questions_from_category(
                            $questiondata->category,
                            !empty($questiondata->questiontext));
                    $randomqids = array_merge($randomqids, $newquestionids);
                    // Remove the original random question, since we're adding all subquestions in it's place.
                    unset ($quizquestions[$questionid]);
                }
            }
            $randomqids = array_unique($randomqids);
            if ($randomqids) {
                $randomsubquestions = question_load_questions($randomqids);
            } else {
                $randomsubquestions = array();
            }

            foreach ($quizquestions as $questionid => $questiondata) {
                $this->_questions[$questionid] = question_bank::make_question($questiondata);
            }
            foreach ($randomsubquestions as $questionid => $questiondata) {
                $this->_questions[$questionid] = question_bank::make_question($questiondata);
                $this->_questions[$questionid]->name = get_string('randomsubquestionname', 'quiz_fbmanager', $this->_questions[$questionid]->name);
            }
        }

        return $this->_questions;
    }

    function get_quiz(){
        if (!isset($this->_quiz)) {
            global $USER;

            $cm = get_coursemodule_from_id('quiz', $this->context->instanceid);
            $this->_quiz = quiz::create($cm->instance, $USER->id);
        }

        return $this->_quiz;
    }

    /**
     * Get the quiz DB record
     * @return object
     */
    function get_quiz_definition(){
        return $this->get_quiz()->get_quiz();
    }

    function get_typename(){
        return 'quiz';
    }

    function has_multiple_user_attempts(){
        $numattempts = $this->get_quiz()->get_num_attempts_allowed();
        return ($numattempts != 1);
    }

    function send_feedback(array $users, array $fbdata){
        //$this->update_quiz_grades();
        return parent::send_feedback($users, $fbdata);
    }

    function get_attempt_groups() {
        global $DB;
        $qubaids = $this->get_qubaid_condition(array());
        $sql = "SELECT DISTINCT quiza.userid
        FROM {$qubaids->from_question_attempts('qa')}
        WHERE {$qubaids->where()}
        ";
        $userids = $DB->get_records_sql($sql, $qubaids->from_where_params());
        $userids = array_keys($userids);
        $gm = $this->get_groupmanager();
        $groups = array();
        foreach ($userids as $userid) {
            $group = $gm->get_user_group($userid);
            $groups[$group->get_uniqueid()] = $group;
        }
        return $groups;
    }

    function update_quiz_grades(){
        // Update quiz grades
        $quiz = $this->get_quiz_definition();
        quiz_update_all_attempt_sumgrades($quiz);
        quiz_update_all_final_grades($quiz);
        quiz_update_grades($quiz);

        // Update group grades
        if ($this->has_grouping()) {
            $grade_item = grade_item::fetch(array(
                     'itemtype' => 'mod',
                     'itemmodule' => 'quiz',
                     'iteminstance' => $this->get_quiz_definition()->id,
                     'courseid' => $this->get_course()->id));
            if ($grade_item) {
                $fmgroups = $this->get_attempt_groups();
                foreach ($fmgroups as $fmgroup) {
                    $attempts = $this->get_group_attempts($fmgroup);
                    $finalgrade = $this->calculate_best_grade($attempts);
                    $finalgrade = quiz_rescale_grade($finalgrade, $quiz, false);
                    foreach ($fmgroup->members as $user) {
                        $grade_item->update_final_grade($user->id, $finalgrade, 'gradebook', get_string('gradeassignedtogroup', 'quiz_fbmanager', $fmgroup->get_name()), FORMAT_PLAIN);
                    }
                }
            }
        }
    }

    /**
     * Get all finished attempts for group, sorted by timefinish
     *
     * @param local_fm_group $group
     */
    function get_group_attempts(local_fm_group $group) {
        global $DB, $CFG;
        // For constants
        require_once($CFG->dirroot . '/mod/quiz/locallib.php');

        if (empty($group->members)) {
            return array();
        }

        $params = array();
        $statuscondition = ' AND state IN (:state1, :state2)';
        $params['state1'] = quiz_attempt::FINISHED;
        $params['state2'] = quiz_attempt::ABANDONED;
        $params['quizid'] = $this->get_quiz_definition()->id;
        $previewclause = ' AND preview = 0';

        $userids = array();
        foreach ($group->members as $user) {
            $userids[] = $user->id;
        }

        list($usersql, $userparams) = $DB->get_in_or_equal($userids, SQL_PARAMS_NAMED);
        $params += $userparams;

        return $DB->get_records_select('quiz_attempts',
                'quiz = :quizid AND userid '. $usersql . $previewclause . $statuscondition,
                $params, 'timefinish ASC');
    }

    function get_grading_options() {
        return array(
                self::GRADEHIGHEST => get_string('gradehighest', 'quiz'),
                self::GRADEAVERAGE => get_string('gradeaverage', 'quiz'),
                self::ATTEMPTFIRST => get_string('attemptfirst', 'quiz'),
                self::ATTEMPTLAST  => get_string('attemptlast', 'quiz')
        );
    }

    function get_grademethod() {
        return $this->get_instance_settings()->grademethod;
    }

    /**
     * Calculate the overall grade for a quiz given a number of attempts by a particular group.
     *
     * @param array $attempts an array of all the user's attempts at this quiz in order.
     * @return float          the overall grade
     */
    function calculate_best_grade($attempts) {

        switch ($this->get_grademethod()) {

            case self::ATTEMPTFIRST:
                $firstattempt = reset($attempts);
                return $firstattempt->sumgrades;

            case self::ATTEMPTLAST:
                $lastattempt = end($attempts);
                return $lastattempt->sumgrades;

            case self::GRADEAVERAGE:
                $sum = 0;
                $count = 0;
                foreach ($attempts as $attempt) {
                    if (!is_null($attempt->sumgrades)) {
                        $sum += $attempt->sumgrades;
                        $count++;
                    }
                }
                if ($count == 0) {
                    return null;
                }
                return $sum / $count;

            case self::GRADEHIGHEST:
            default:
                $max = null;
                foreach ($attempts as $attempt) {
                    if ($attempt->sumgrades > $max) {
                        $max = $attempt->sumgrades;
                    }
                }
                return $max;
        }
    }

}

?>
