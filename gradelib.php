<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Feedback manager quiz report.
 *
 * @package     quiz_fbmanager
 * @copyright   2014 University of Wisconsin
 * @author      Nick Koeppen, Matt Petro
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot.'/mod/quiz/report/fbmanager/forms.php');

class quiz_fbmanager_grade_report extends quiz_fbmanager_report_base {

    protected $_form;
    protected $_preview;

    function extend_navigation(moodle_page $page){
        $url = $page->url;
        $url->params($this->get_page_params());
        $page->navbar->add(get_string('gradetabname','quiz_fbmanager'), $url);
    }

    /**
     *
     * @return local_fm_send_form
     */
    function get_grade_form(){
        global $PAGE;

        if (!isset($this->_form)) {
            $customdata = array('manager' => $this->manager);
            $this->_form = new quiz_fbmanager_grade_form($PAGE->url, $customdata);
        }

        return $this->_form;
    }

    function process_submission(){
        global $PAGE;

        $sendform = $this->get_grade_form();
        $return   = $PAGE->url;

        if ($sendform->is_cancelled()) {
            redirect($return);
        } else if ($data = $sendform->get_data()) {
            $this->manager->update_quiz_grades();
            $message = get_string('updatedquizgrades', 'quiz_fbmanager');
            redirect($return, $message);
        }
    }

    function has_overlapping_groups() {
        return $this->manager->get_groupmanager()->has_overlapping_groups();
    }

}