<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * AJAX callback for assigning feedback.
 *
 * @package     quiz_fbmanager
 * @copyright   2014 University of Wisconsin
 * @author      Nick Koeppen, Matt Petro
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

define('AJAX_SCRIPT', true);

require_once('../../../../config.php');
require_once($CFG->dirroot.'/mod/quiz/report/default.php');
require_once($CFG->dirroot.'/mod/quiz/report/reportlib.php');
require_once($CFG->dirroot.'/mod/quiz/report/fbmanager/report.php');
require_once($CFG->dirroot.'/mod/quiz/report/fbmanager/assignlib.php');

$cmid = required_param('cmid', PARAM_INT);
$questionid = required_param('questionid', PARAM_INT);

global $PAGE, $OUTPUT, $USER;

$PAGE->set_url(new moodle_url('/mod/quiz/report/fbmanager/ajax.php'));

$context = context_module::instance($cmid);
$PAGE->set_context($context);

$manager = new quiz_fm_manager($context, $questionid);
$manager->verify_page();

$controller = new local_fm_assign_ajax_controller($manager);
require_capability('quiz/fbmanager:manage', $manager->get_context());
require_sesskey();

echo $OUTPUT->header(); // send headers

$result = array();
try {
    $controller->process_submission();
} catch (local_fm_invalid_input_exception $e) {
    $result['validationerror'] = $e->getMessage();
}
$result['cells'] = $controller->get_ajax_response();
echo json_encode($result);
die();
