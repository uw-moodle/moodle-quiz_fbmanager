YUI.add('moodle-quiz_fbmanager-preview', function(Y) {

M.quiz_fbmanager = M.quiz_fbmanager || {};
/**
 * Popup a new window for email preview
 *
 * @param {object} config
 */
M.quiz_fbmanager.preview_popup = function(config) {
    Y.one('#quiz_fbmanager_send_form #id_preview').on('click', function(e){
        // Construct the preview url based on the form contents
        var regexp = /^(.*)\[(.*)\]$/;
        var assigners = [];
        var questions = [];
        Y.all('input[type=checkbox]').each(function(checkbox) {
            if (checkbox.get('checked')) {
                var name = checkbox.get('name');
                var match = regexp.exec(name);
                if (match) {
                    if (match[1] == 'assigners') {
                        assigners.push(match[2]);
                    } else if (match[1] == 'questions') {
                        questions.push(match[2]);
                    }
                }
            }
        });
        config.url = config.baseurl + '&assigners=' + assigners.join(',') + '&questions=' + questions.join(',');
        // Open a popup to the preview page
        openpopup(e, config);
        }, this);
    };

}, '@VERSION@', {'requires':['base','node']});
