<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Feedback manager quiz report.
 *
 * @package     quiz_fbmanager
 * @copyright   2014 University of Wisconsin
 * @author      Nick Koeppen, Matt Petro
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->libdir . '/formslib.php');

/**
 * Form to update quiz grades.
 * @author Matt Petro
 */
class quiz_fbmanager_grade_form extends moodleform {

    /**
     * A feedback manager
     * @var local_fm_manager
     */
    protected $manager;

    function definition(){
        $mform = $this->_form;

        $this->manager = $this->_customdata['manager'];

        $buttonarray=array();
        $buttonarray[] = &$mform->createElement('submit', 'submit', get_string('updatequizgrades', 'quiz_fbmanager'), array('class'=>'form-submit'));
        $mform->addGroup($buttonarray, 'buttonar', '', array(' '), false);
        $mform->closeHeaderBefore('buttonar');
    }
}


/**
 * Configuration of feedback report for a quiz.
 *
 * @author Matt Petro
 */

class quiz_fbmanager_settings_form extends moodleform {

    /**
     * A feedback manager
     * @var quiz_fm_manager
     */
    protected $manager;

    function definition() {
        global $DB, $COURSE;

        $mform =& $this->_form;

        $this->manager = $this->_customdata['manager'];

        $mform->addElement('header', 'groups', get_string('groupsettings', 'quiz_fbmanager'));

        $mform->addElement('checkbox', 'groupmode', get_string('groupmode', 'quiz_fbmanager'));
        $mform->addHelpButton('groupmode', 'groupmode', 'quiz_fbmanager');

        $options = array();
        $options[0] = get_string('none');
        if ($groupings = $DB->get_records('groupings', array('courseid'=>$COURSE->id))) {
            foreach ($groupings as $grouping) {
                $options[$grouping->id] = format_string($grouping->name);
            }
        }
        $mform->addElement('select', 'groupingid', get_string('grouping', 'group'), $options);
        $mform->addHelpButton('groupingid', 'grouping', 'quiz_fbmanager');
        $mform->disabledIf('groupingid', 'groupmode', 'notchecked');

        // Grading method.
        $mform->addElement('select', 'grademethod', get_string('grademethod', 'quiz_fbmanager'),
                $this->manager->get_grading_options());
        $mform->addHelpButton('grademethod', 'grademethod', 'quiz_fbmanager');
        $mform->setDefault('grademethod', quiz_fm_manager::GRADEHIGHEST);
        $mform->disabledIf('grademethod', 'groupmode', 'notchecked');

        $this->add_action_buttons(false);
    }

    function set_data($default_values) {
        // Groupingid = null means we are not using groupmode.
        if (!is_null($default_values->groupingid)) {
            $default_values->groupmode = 1;
        } else {
            $default_values->groupmode = 0;
        }
        return parent::set_data($default_values);
    }
}