<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Feedback manager quiz report.
 *
 * @package     quiz_fbmanager
 * @copyright   2014 University of Wisconsin
 * @author      Nick Koeppen, Matt Petro
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

/**
 * Quiz overview report upgrade function.
 * @param number $oldversion
 */
function xmldb_quiz_fbmanager_upgrade($oldversion) {
    global $CFG, $DB;

    $dbman = $DB->get_manager();

    if ($oldversion < 2013121300) {

        // Define table quiz_fbmanager to be created
        $table = new xmldb_table('quiz_fbmanager');

        // Adding fields to table quiz_fbmanager
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('quizid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $table->add_field('groupingid', XMLDB_TYPE_INTEGER, '10', null, null, null, null);

        // Adding keys to table quiz_fbmanager
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));
        $table->add_key('quizid', XMLDB_KEY_FOREIGN, array('quizid'), 'quiz', array('id'));
        $table->add_key('groupingid', XMLDB_KEY_FOREIGN, array('groupingid'), 'groupings', array('id'));

        // Conditionally launch create table for quiz_fbmanager
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }

        // fbmanager savepoint reached
        upgrade_plugin_savepoint(true, 2013121300, 'quiz', 'fbmanager');
    }

    if ($oldversion < 2013121700) {

        // Define field grademethod to be added to quiz_fbmanager
        $table = new xmldb_table('quiz_fbmanager');
        $field = new xmldb_field('grademethod', XMLDB_TYPE_INTEGER, '4', null, null, null, null, 'groupingid');

        // Conditionally launch add field grademethod
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        // fbmanager savepoint reached
        upgrade_plugin_savepoint(true, 2013121700, 'quiz', 'fbmanager');
    }

    return true;
}
