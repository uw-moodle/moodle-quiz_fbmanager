<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Feedback manager quiz report.
 *
 * @package     quiz_fbmanager
 * @copyright   2014 University of Wisconsin
 * @author      Nick Koeppen, Matt Petro
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot.'/local/fm/assign/forms.php');

class quiz_fbmanager_assign_report extends quiz_fbmanager_report_base {
    /**
     * The extended fbmanager assign table.
     * @var quiz_fm_assign_table
     */
    protected $table;

    function __construct(quiz_fm_manager $manager){
        parent::__construct($manager);

        $this->table = new quiz_fm_assign_table($this->manager);
    }

    function check_permissions(){
        parent::check_permissions();
        require_capability('mod/quiz:grade', $this->manager->get_context());
    }

    function extend_navigation(moodle_page $page){
        $page->navbar->add(get_string('assigntabname', 'quiz_fbmanager'), $page->url);
    }

    function get_page_params(){
        return $this->table->get_page_params();
    }

    function get_table(){
        return $this->table;
    }

    function setup_page(moodle_page $page){
        parent::setup_page($page);

        $this->table->define_baseurl();
        $this->table->setup();
    }

    function process_submission() {
        $this->table->process_submission();
    }
}

class quiz_fm_assign_table extends local_fm_assign_table {

    /**
     * Uses the quiz feedback manager (for auto hinting)
     * @var quiz_fbmanager
     */
    protected $manager;

    function get_component($name){
        $classname = 'quiz_fm_assign_'.$name;
        if (class_exists($classname)) {
            return new $classname($this);
        }

        return parent::get_component($name);
    }

    function get_quiz(){
        return $this->manager->get_quiz();
    }

}
