<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Feedback manager quiz report.
 *
 * @package     quiz_fbmanager
 * @copyright   2014 University of Wisconsin
 * @author      Nick Koeppen, Matt Petro
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['pluginname'] = 'Feedback Manager';

$string['assigntab'] = 'Assign feedback';
$string['assigntabname'] = 'Assign feedback';
$string['editmanual'] = 'Edit (manual)';
$string['edittemplate'] = 'Edit template';
$string['fbmanager'] = 'Feedback Manager';
$string['fbmanager:viewreports'] = 'View feedback manager report';
$string['fbmanager:viewstudentnames'] = 'View student names';
$string['fbmanager:manage'] = 'Manage all feedback';
$string['fbmanager:messageconfig'] = 'Manage message configuration (unused)';
$string['feedback'] = 'Feedback';
$string['feedbackassign'] = 'Assign feedback';
$string['feedbackassign_help'] = '<p>Use this page to assign feedback and grades to quiz attempts.  Feedback can be assigned to attempts individually, or the feedback bank at the left can be used to generate standard feedback which can be assigned quickly to many attempts.</p>';
$string['feedbackassigncount'] = '(Given to {$a} student responses)';
$string['feedbackreport'] = 'Instructor feedback report';
$string['feedbackreportintro'] = '<p>This feedback report displays a summary of feedback provided
to students.  The report includes any question for which there is there is at least one student answer marked to show in the report.
Following each question is the instructor feedback and marked student responses.  The report can be grouped by student response or by feedback item.</p>
<p> You may choose to save and manually edit this report by clicking "Save".  This stores a snapshot
of the report which can be referred to later.  If you want the students to view the report, then you may wish to copy the text of the report to a
page resource on the course page.</p>';
$string['feedbacksenderror'] = 'A failure occurred.  You can try resending the feedback to retry any unsent messages.';
$string['feedbacksendok'] = 'SENT!';
$string['gradeassignedtogroup'] = 'Grade assigned to group {$a}';
$string['grademethod'] = 'Grading method';
$string['grademethod_help'] = 'When there are multiple attempts in the same group, the following methods are available for calculating the final quiz grade:

* Highest grade of all attempts
* Average (mean) grade of all attempts
* First attempt (all other attempts are ignored)
* Last attempt (all other attempts are ignored)';
$string['gradetab'] = 'Update gradebook';
$string['gradetabname'] = 'Update gradebook';
$string['groupbyfeedback'] = 'Group by feedback';
$string['groupbyresponse'] = 'Group by response';
$string['grouping'] = 'Grouping';
$string['grouping_help'] = 'A grouping is a collection of groups within a course. If a grouping is selected, then only groups in that grouping will be considered.';
$string['groupmode'] = 'Students worked in groups';
$string['groupmode_help'] = '<p>Choose this option when students took the quiz in a group and you wish the same feedback and grade to be given to all students in the group.
        You will also need to select a grouping which contains all student groups.  The groups should be disjoint; in the case that one student is in two different groups, the behavior is undefined.</p>';
$string['groupsettings'] = 'Group settings';
$string['groupsnotdisjoint'] = 'Warning: The configured grouping has overlapping groups which may result in feedback and grades being incorrectly assigned.  Please check the grouping configuration.';
$string['groupsummary'] = 'Summary of previous attempts (Group work)';
$string['managefeedback'] = 'Manage feedback';
$string['managefeedback_help'] = 'This report allows an instructor to assign feedback and manually grade student responses to the quiz.';
$string['managefeedbackfor'] = 'Manage feedback for {$a}';
$string['notypicalfeedback'] = 'No student responses have been marked for reporting on the assign feedback page.';
$string['organization'] = 'Organization';
$string['preview'] = 'Preview';
$string['preview_help'] = 'Select a user to see the feedback email they will receive.';
$string['previewfeedback'] = 'Preview feedback';
$string['randomsubquestionname'] = 'Random: {$a}';
$string['regenerate'] = 'Regenerate';
$string['reportoptions'] = 'Report options';
$string['reporttab'] = 'View report';
$string['reporttabname'] = 'View report';
$string['reporttype'] = 'Report type';
$string['responseswithnofeedback'] = 'Other student responses';
$string['savereport'] = 'Save report for editing';
$string['sendfeedback'] = 'Send feedback to students';
$string['sendgradesintro'] = '<p>Clicking <strong>Update gradebook</strong> will update the moodle gradebook to
include all grades modified for this quiz in Feedback manager. When grading is complete, be sure to click this button so that students will
receive their correct grade.</p>';
$string['sendfeedbackintro'] = '<p>This page handles sending of feedback to students.  The feedback is sent to each student via email
as well as saved in the quiz attempt itself so that it is visible inside moodle. Students will only receive their feedback once feedback is sent using this page.</p>';
$string['sendtab'] = 'Send feedback';
$string['sendtabname'] = 'Send feedback';
$string['settingstab'] = 'Settings';
$string['settingstabname'] = 'Settings';
$string['showallfeedback'] = 'Show all feedback';
$string['showallfeedback_help'] = 'If this is checked, all feedback assigned is included in the report.  If not checked, only feedback marked for reporting is included.';
$string['showcustomfeedback'] = 'Include individual feedback';
$string['showallresponses'] = 'Show all responses';
$string['showallresponses_help'] = 'If this is checked, all responses are included in the report, regardless of whether feedback is assigned or not.';
$string['showfbcount'] = 'Show feedback count';
$string['showfbcount_help'] = 'Show the number of responses marked with each feedback item.';
$string['showgrades'] = 'Show grades';
$string['showgrades_help'] = 'Show grade next to each response.';
$string['showgroups'] = 'Show group names';
$string['showusers'] = 'Show user names';
$string['showusers_help'] = 'Shows name next to each response.';
$string['studentresponses'] = 'Student responses';
$string['typicalstudentresponses'] = 'Typical student responses';
$string['updatedquizgrades'] = 'Updated gradebook';
$string['updatequizgrades'] = 'Update gradebook';