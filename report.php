<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Feedback manager quiz report.
 *
 * @package     quiz_fbmanager
 * @copyright   2014 University of Wisconsin
 * @author      Nick Koeppen, Matt Petro
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot."/mod/quiz/report/fbmanager/lib.php");
require_once($CFG->dirroot."/mod/quiz/report/fbmanager/renderer.php");

class quiz_fbmanager_report extends quiz_default_report {
    const PARAM_DISPLAY = 'display';
    const QUESTIONID_SESSION_VAR = 'quiz_fbmanager_qid';

    const DISPLAY_ASSIGN   = 'assign';
    const DISPLAY_SEND     = 'send';
    const DISPLAY_GRADE    = 'grade';
    const DISPLAY_REPORT   = 'report';
    const DISPLAY_SETTINGS = 'settings';

    protected $quiz;

    function display($quiz, $cm, $course) {
        global $CFG, $PAGE, $OUTPUT, $SESSION;

        $this->quiz = $quiz;

        $displaytype = optional_param(static::PARAM_DISPLAY, self::DISPLAY_ASSIGN, PARAM_ALPHA);

        $questionid  = optional_param('qid', null, PARAM_INT);
        // persist the questionid in the user's session
        if ($questionid) {
            $SESSION->{static::QUESTIONID_SESSION_VAR}[$quiz->id] = $questionid;
        } else if (!empty($SESSION->{static::QUESTIONID_SESSION_VAR}[$quiz->id])) {
            $questionid = $SESSION->{static::QUESTIONID_SESSION_VAR}[$quiz->id];
        }

        // Context and capabilites
        $context = context_module::instance($cm->id);
        $manager = new quiz_fm_manager($context, $questionid);

        switch($displaytype){
            case self::DISPLAY_SEND:
                require_once($CFG->dirroot.'/mod/quiz/report/fbmanager/sendlib.php');
                $report = new quiz_fbmanager_send_report($manager);
                break;
            case self::DISPLAY_GRADE:
                require_once($CFG->dirroot.'/mod/quiz/report/fbmanager/gradelib.php');
                $report = new quiz_fbmanager_grade_report($manager);
                break;
            case self::DISPLAY_REPORT:
                require_once($CFG->dirroot.'/mod/quiz/report/fbmanager/reviewlib.php');
                $report = new quiz_fbmanager_feedback_report($manager);
                break;
            case self::DISPLAY_SETTINGS:
                require_once($CFG->dirroot.'/mod/quiz/report/fbmanager/settingslib.php');
                $report = new quiz_fbmanager_settings_report($manager);
                break;
            case self::DISPLAY_ASSIGN:
            default:
                require_once($CFG->dirroot.'/mod/quiz/report/fbmanager/assignlib.php');
                $report = new quiz_fbmanager_assign_report($manager);
                break;
        }
        $report->check_permissions();

        // Navigation
        $baseurl = new moodle_url('/mod/quiz/report.php');
        $params = array('id' => $cm->id, 'mode' => 'fbmanager');
        $navbase = new moodle_url($baseurl, $params);
        $params[static::PARAM_DISPLAY] = $displaytype;
        $params = array_merge($params, $report->get_page_params());
        $PAGE->set_url($baseurl, $params);

        navigation_node::override_active_url($navbase);
        $report->extend_navigation($PAGE);

        // Setup page
        $report->setup_page($PAGE);

        // Data processing
        $report->process_submission();

        // Display page
        echo $OUTPUT->header();

        echo $OUTPUT->heading($PAGE->heading);

        $this->print_tabs($displaytype);

        $report->render($PAGE);
    }

    protected function print_tabs($displaytype){
        global $PAGE, $OUTPUT;

        $row = array();
        $url = $PAGE->url;
        $url->param(static::PARAM_DISPLAY, static::DISPLAY_ASSIGN);
        $row[] = new tabobject(static::DISPLAY_ASSIGN, $url, get_string('assigntab', 'quiz_fbmanager'));
        $url = $PAGE->url;
        $url->param(static::PARAM_DISPLAY, static::DISPLAY_SEND);
        $row[] = new tabobject(static::DISPLAY_SEND, $url, get_string('sendtab', 'quiz_fbmanager'));
        $url = $PAGE->url;
        $url->param(static::PARAM_DISPLAY, static::DISPLAY_GRADE);
        $row[] = new tabobject(static::DISPLAY_GRADE, $url, get_string('gradetab', 'quiz_fbmanager'));
        $url = $PAGE->url;
        $url->param(static::PARAM_DISPLAY, static::DISPLAY_REPORT);
        $row[] = new tabobject(static::DISPLAY_REPORT, $url, get_string('reporttab', 'quiz_fbmanager'));
        $url = $PAGE->url;
        $url->param(static::PARAM_DISPLAY, static::DISPLAY_SETTINGS);
        $row[] = new tabobject(static::DISPLAY_SETTINGS, $url, get_string('settingstab', 'quiz_fbmanager'));

        echo $OUTPUT->container_start('fbmanager_report_navigation');

        print_tabs(array($row), $displaytype);

        echo $OUTPUT->container_end();
    }

}

abstract class quiz_fbmanager_report_base implements renderable {
    const DEFAULT_PAGE_SIZE = 30;

    protected $manager;

    function __construct(quiz_fm_manager $manager){
        $this->manager = $manager;
    }

    function check_permissions(){
        require_capability('quiz/fbmanager:manage', $this->manager->get_context());
    }

    function extend_navigation(moodle_page $page){
        return;
    }

    function get_context(){
        return $this->manager->get_context();
    }

    function get_manager(){
        return $this->manager;
    }

    function get_cm(){
        return $this->manager->get_cm();
    }

    function get_page_params(){
        return array();
    }

    function process_submission(){
        return;
    }

    function render(moodle_page $page){
        $renderer = $this->manager->get_page_renderer($page);

        $renderer->render($this);
    }

    function setup_page(moodle_page $page){
        global $OUTPUT;
        $page->set_pagelayout('base');
        $quizname = $this->manager->get_quiz()->get_quiz_name();
        $title = get_string('managefeedbackfor', 'quiz_fbmanager', $quizname);
        $page->set_title(format_string($title));
        $page->set_heading($page->title);
    }
}