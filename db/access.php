<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Feedback manager quiz report.
 *
 * @package     quiz_fbmanager
 * @copyright   2014 University of Wisconsin
 * @author      Nick Koeppen, Matt Petro
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$capabilities = array(
    'quiz/fbmanager:viewreports' => array(
            'captype' => 'read',
            'contextlevel' => CONTEXT_MODULE,
            'legacy' => array(
                    'teacher' => CAP_ALLOW,
                    'editingteacher' => CAP_ALLOW
            ),
            'clonepermissionsfrom' =>  'mod/quiz:viewreports'
    ),
    'quiz/fbmanager:viewstudentnames' => array(
            'captype' => 'read',
            'contextlevel' => CONTEXT_MODULE,
            'legacy' => array(
                    'manager' => CAP_ALLOW,
                    'teacher' => CAP_ALLOW,
                    'editingteacher' => CAP_ALLOW
            ),
    ),
    'quiz/fbmanager:manage' => array(
            'captype' => 'write',
            'contextlevel' => CONTEXT_COURSE,
            'archetypes' => array(
                'manager' => CAP_ALLOW,
                'editingteacher' => CAP_ALLOW,
            )
    ),
    'quiz/fbmanager:messageconfig' => array(
            'captype' => 'write',
            'contextlevel' => CONTEXT_MODULE,
            'archetypes' => array(
                'manager' => CAP_ALLOW,
                'editingteacher' => CAP_ALLOW,
            )
    ),
);

