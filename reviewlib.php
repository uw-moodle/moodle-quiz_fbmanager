<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Feedback manager quiz report.
 *
 * @package     quiz_fbmanager
 * @copyright   2014 University of Wisconsin
 * @author      Nick Koeppen, Matt Petro
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot.'/local/fm/report/lib.php');
require_once($CFG->dirroot.'/local/fm/report/forms.php');

class quiz_fbmanager_feedback_report extends quiz_fbmanager_report_base {

    /**
     * Report definition
     * @var local_fm_report
     */
    protected $_definition;

    function process_submission() {
        global $PAGE;
        $settingform = $this->get_settings_form();

        if ($settingform->edit_report()) {
            $form = $this->get_definition_edit_form();
            if ($form->is_cancelled()) {
                redirect($PAGE->url);
            } else if ($data = $form->get_data()) {
                $definition = $this->get_definition();
                $definition->form_load_data($data);
                $definition->save();

                redirect($PAGE->url);
            }
        }
        return true;
    }

    function get_data_for_display(){
        $data = array();
        $showallfeedback = $this->show_allfeedback();
        $showcustomfeedback = $this->show_customfeedback();
        $showallresponses = $this->show_allresponses();
        foreach($this->manager->get_questions() as $qid => $question){
            $data[$qid]['question'] = $question;
            $assignments = $this->manager->get_assignments($qid);
            foreach($this->manager->get_assignments($qid) as $qaid => $assignments){
                $data[$qid]['feedback'][$qaid] = array();
                $assigns = array_merge($assignments->get_feedback_assigns('sent'), $assignments->get_feedback_assigns('current'));
                if ($showcustomfeedback) {
                    $assigns = array_merge($assigns, $assignments->get_feedback_custom('sent'), $assignments->get_feedback_custom('current'));
                }
                $assigns = array_merge($assignments->get_feedback_assigns('sent'), $assignments->get_feedback_assigns('current'));
                foreach($assigns as $assign){
                    if (!$assign->is_typical() && !$showallfeedback) {
                        continue;
                    }
                    $attempt = $assignments->get_question_attempt();
                    $fbiid = $assign->get_feedback_instance()->id;
                    $data[$qid]['feedbackbyresponse'][$qaid]['attempt'] = $attempt;
                    $data[$qid]['feedbackbyresponse'][$qaid]['assigns'][] = $assign;
                    $data[$qid]['responsebyfeedback'][$fbiid]['assign'] = $assign;
                    $data[$qid]['responsebyfeedback'][$fbiid]['attempts'][] = $attempt;
                }
                if ($showcustomfeedback) {
                    $customs = array_merge($assignments->get_feedback_custom('sent'), $assignments->get_feedback_custom('current'));
                    foreach ($customs as $custom) {
                        $attempt = $assignments->get_question_attempt();
                        $customid = $custom->get_db_record()->id;
                        $data[$qid]['feedbackbyresponse'][$qaid]['attempt'] = $attempt;
                        $data[$qid]['feedbackbyresponse'][$qaid]['customs'][] = $custom;
                        $data[$qid]['responsebycustom'][$customid]['custom'] = $custom;
                        $data[$qid]['responsebycustom'][$customid]['attempts'][] = $attempt;
                    }
                }
                if (!isset($data[$qid]['feedbackbyresponse'][$qaid]['attempt']) && $showallresponses) {
                    $attempt = $assignments->get_question_attempt();
                    $data[$qid]['feedbackbyresponse'][$qaid]['attempt'] = $attempt;
                    $data[$qid]['responsenofeedback']['attempts'][] = $attempt;
                }
            }
        }

        return $data;
    }

    function extend_navigation(moodle_page $page){
        $page->navbar->add(get_string('reporttabname', 'quiz_fbmanager'), $page->url);
    }

    /**
     * Get the report definition (saved version of a report).
     * @return local_fm_report
     */
    function get_definition(){
        if (!isset($this->_definition)) {
            $this->_definition = $this->manager->get_report();
        }

        return $this->_definition;
    }

    function get_definition_edit_form(){
        global $PAGE;
        $definition = $this->get_definition();

        $submiturl = $PAGE->url->out(false, array('edit' => true));
        $form = new local_fm_report_edit_form($submiturl, array('report' => $definition));

        return $form;
    }

    function get_quiz(){
        return $this->manager->get_quiz_definition();
    }

    function get_settings_form(){
        global $PAGE;

        $definition = $this->get_definition();

        $form = new quiz_fbmanager_review_form($PAGE->url, array('report' => $this));
        if ($formdata = $form->get_data()) {
            if (isset($formdata->delete)) {
                $definition->delete();
                unset($this->_definition);

                redirect($PAGE->url);
            } else if (!isset($formdata->edit)) {
                if ((isset($formdata->save) || !$definition->exists())) {
                    // Partial report definition
                    $newdef = new stdClass();
                    $newdef->config = new stdClass();
                    $newdef->config->showgrades = $formdata->showgrades;
                    $newdef->config->showfbcount = $formdata->showfbcount;
                    $newdef->config->showusers = $formdata->showusers;
                    $newdef->config->showallfeedback = $formdata->showallfeedback;
                    $newdef->config->showcustomfeedback = $formdata->showcustomfeedback;
                    $newdef->config->showallresponses = $formdata->showallresponses;
                    $newdef->config->type = $formdata->type;
                    $definition->form_load_data($newdef);
                }
            }
        }

        return $form;
    }

    function is_editable(){
        return $this->get_definition()->exists();
    }

    function get_config() {
        return $this->get_definition()->get_config();
    }

    function show_feedback_count(){
        $config = $this->get_config();
        return !empty($config->showfbcount);
    }

    function show_grades(){
        $config = $this->get_config();
        return !empty($config->showgrades);
    }

    function show_users(){
        $config = $this->get_config();
        return !empty($config->showusers);
    }

    function show_allfeedback(){
        $config = $this->get_config();
        return !empty($config->showallfeedback);
    }

    function show_customfeedback(){
        $config = $this->get_config();
        return !empty($config->showcustomfeedback);
    }


    function show_allresponses(){
        $config = $this->get_config();
        return !empty($config->showallresponses);
    }

    function get_report_type(){
        $config = $this->get_config();
        return !empty($config->type) ? $config->type : local_fm_report::TYPE_FEEDBACK;
    }
}

class quiz_fbmanager_review_form extends moodleform {

    protected $datacache;

    function definition(){
        $mform = $this->_form;

        $mform->disable_form_change_checker();

        $report = $this->_customdata['report'];
        $manager = $report->get_manager();
        $groupmode = $manager->get_groupingid();

        $mform->addElement('header', 'configheader', get_string('reporttype', 'quiz_fbmanager'));
        $types = array(local_fm_report::TYPE_FEEDBACK => get_string('groupbyfeedback', 'quiz_fbmanager'),
                local_fm_report::TYPE_RESPONSE => get_string('groupbyresponse', 'quiz_fbmanager'));
        $mform->addElement('select', 'type', get_string('organization', 'quiz_fbmanager'), $types);
        $mform->addElement('advcheckbox', 'showallresponses', '', get_string('showallresponses', 'quiz_fbmanager'));
        $mform->addHelpButton('showallresponses', 'showallresponses', 'quiz_fbmanager');

        $mform->addElement('advcheckbox', 'showallfeedback', '', get_string('showallfeedback', 'quiz_fbmanager'));
        $mform->addHelpButton('showallfeedback', 'showallfeedback', 'quiz_fbmanager');
        $mform->addElement('advcheckbox', 'showcustomfeedback', '', get_string('showcustomfeedback', 'quiz_fbmanager'));
        $mform->disabledIf('showcustomfeedback', 'showallfeedback', 'notchecked');

        $mform->addElement('header', 'configheader', get_string('reportoptions', 'quiz_fbmanager'));
        $mform->addElement('advcheckbox', 'showgrades', '', get_string('showgrades', 'quiz_fbmanager'));
        $mform->addHelpButton('showgrades', 'showgrades', 'quiz_fbmanager');
        $mform->addElement('advcheckbox', 'showfbcount', '', get_string('showfbcount', 'quiz_fbmanager'));
        $mform->addHelpButton('showfbcount', 'showfbcount', 'quiz_fbmanager');
        if ($groupmode) {
            $showuserstr = get_string('showgroups', 'quiz_fbmanager');
        } else {
            $showuserstr = get_string('showusers', 'quiz_fbmanager');
        }
        $mform->addElement('advcheckbox', 'showusers', '', $showuserstr);
        $mform->addHelpButton('showusers', 'showusers', 'quiz_fbmanager');

        $mform->setDefault('showgrades', $report->show_grades());
        $mform->setDefault('showfbcount', $report->show_feedback_count());
        $mform->setDefault('showusers', $report->show_users());
        $mform->setDefault('showallfeedback', $report->show_allfeedback());
        $mform->setDefault('showallresponses', $report->show_allresponses());
        $mform->setDefault('type', $report->get_report_type());

        $buttonarray=array();
        if ($report->is_editable()) {
            $buttonarray[] = &$mform->createElement('submit', 'delete', get_string('remove'));
            $buttonarray[] = &$mform->createElement('submit', 'save', get_string('regenerate', 'quiz_fbmanager'));
            $buttonarray[] = &$mform->createElement('submit', 'edit', get_string('editmanual', 'quiz_fbmanager'));
        } else {
            $buttonarray[] = &$mform->createElement('submit', 'refresh', get_string('refresh'));
            $buttonarray[] = &$mform->createElement('submit', 'save', get_string('savereport', 'quiz_fbmanager'));
        }
        $mform->addGroup($buttonarray, 'buttonar', '', array(' '), false);
        $mform->closeHeaderBefore('buttonar');
    }

    function edit_report(){
        $data = $this->get_data();
        return isset($data->edit) || optional_param('edit', false, PARAM_BOOL);
    }

    function get_data(){
        if (!isset($this->datacache)) {
            $this->datacache = parent::get_data();
        }

        return $this->datacache;
    }

    function save_report(){
        $data = $this->get_data();
        return isset($data->save);
    }
}