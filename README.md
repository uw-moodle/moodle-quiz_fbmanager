Group feedback support
======================

Feedback manager has a limited ability to process group quiz attempts (i.e. quiz attempts made by one member of a group,
but graded as if it was made by every member of the group.)  This is really a feature which should be built into the
quiz module itself, but it isn't, so we try to emulate group quizzes in feedback manager.

To to make use of group quizzes, a grouping has to be created by the instructor and configured in the feedback manager settings.
Once configured, grades and feedback emails will be sent to all memeber of a group as if they had taken the quiz themselves.  Group
grades are added to the gradebook as "gradebook overrides" on the quiz grade item.  This means that group quiz grades will only
update manually when grades are submitted in feedback manager.

Moodle patch for group feedback (optional)
------------------------------------------
Apply the included patch to allow group quiz attempts to be reviewed by any member of a group inside moodle using the normal quiz review screen.  This patch is for moodle 2.9, but should work on other versions as well.

`cd moodle`

`patch -p0 < group_quiz.patch`



