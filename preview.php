<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Feedback manager quiz report.
 *
 * @package     quiz_fbmanager
 * @copyright   2014 University of Wisconsin
 * @author      Nick Koeppen, Matt Petro
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once('../../../../config.php');
require_once($CFG->dirroot.'/mod/quiz/report/default.php');
require_once($CFG->dirroot.'/mod/quiz/report/reportlib.php');
require_once($CFG->dirroot.'/mod/quiz/report/fbmanager/report.php');
require_once($CFG->dirroot."/mod/quiz/report/fbmanager/lib.php");
require_once($CFG->dirroot."/mod/quiz/report/fbmanager/sendlib.php");

$cmid = required_param('cmid', PARAM_INT);
$assigneridparam = required_param('assigners', PARAM_SEQUENCE);
$qidparam = required_param('questions', PARAM_SEQUENCE);
$format = optional_param('format', FORMAT_HTML, PARAM_INT);

$assignerids = explode(',', $assigneridparam);
$qids = explode(',', $qidparam);


global $PAGE, $OUTPUT, $USER;

if (!$cm = get_coursemodule_from_id('quiz', $cmid)) {
    print_error('invalidcoursemodule');
}
$context = context_module::instance($cmid);
$course = $DB->get_record('course', array('id' => $cm->course), '*', MUST_EXIST);

// Check login and sesskey.
require_login($course, false, $cm);
require_capability('quiz/fbmanager:manage', $context);

$url = new moodle_url('/mod/quiz/report/fbmanager/preview.php');
$url->param('cmid', $cmid);
$url->param('assigners', $assigneridparam);
$url->param('questions', $qidparam);
$url->param('format', $format);
$PAGE->set_url($url);

$PAGE->set_pagelayout('popup');


$manager = new quiz_fm_manager($context);
$preview = new local_fm_send_preview($manager, $qids, $assignerids, $format);

// Start output.
$title = get_string('previewfeedback', 'quiz_fbmanager');
$PAGE->set_title($title);
$PAGE->set_heading($title);
echo $OUTPUT->header(); // send headers


$renderer = $manager->get_page_renderer($PAGE);

$renderer->render($preview);

echo $OUTPUT->footer();
